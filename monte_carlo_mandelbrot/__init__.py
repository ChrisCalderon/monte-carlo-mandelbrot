# This file is part of Monte Carlo Mandelbrot, a module which generates random points in the Mandelbrot set.
# Copyright (C) 2022  Christian Calderon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# To contact the author you can send an email to calderonchristian73 AT gmail DOT com,
# or you can make an issue on the gitlab repository: https://gitlab.com/ChrisCalderon/monte-carlo-mandelbrot
from numpy.random import default_rng
import numpy as np


def mc_mandelbrot(
        min_real: float,
        max_real: float,
        min_imag: float,
        max_imag: float,
        iterations: int,
        max_points: int
):
    """Checks random points for divergence under the Mandelbrot iteration."""
    # Ignore overflow and invalid value errors caused by repeated iteration
    # of z*z + c. These errors should cause divergent values to have a
    # magnitude of inf, which is fine.
    old_settings = np.seterr(over='ignore', invalid='ignore')    

    rng = default_rng()
    
    # We want the imaginary parts to be in (MIN_IMAG, MAX_IMAG] so that
    # MAX_IMAG will map to pixel row 0 and MIN_IMAG will always map to a
    # value less than IMAGE_HEIGHT.
    
    # The uniform method gives values in [min, max), so to get a range of
    # (MIN_IMAG, MAX_IMAG], take 0 - uniform(-MAX_IMAG, -MIN_IMAG) which
    # gives the range 0 - [-MAX_IMAG, -MIN_IMAG), which is the same as
    # (MIN_IMAG, MAX_IMAG]
    
    c = rng.uniform(min_real, max_real, size=max_points) - \
        rng.uniform(-max_imag, -min_imag, size=max_points)*1j

    z = np.zeros_like(c)
    
    for _ in range(iterations):
        np.multiply(z, z, out=z)
        np.add(z, c, out=z)

    result = c[np.abs(z) <= 2.0]
    np.seterr(**old_settings)
    return result
