# This file is part of Monte Carlo Mandelbrot, a module which generates random points in the Mandelbrot set.
# Copyright (C) 2022  Christian Calderon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# To contact the author you can send an email to calderonchristian73 AT gmail DOT com,
# or you can make an issue on the gitlab repository: https://gitlab.com/ChrisCalderon/monte-carlo-mandelbrot
class BitMatrix:
    """Store a packed bit matrix in the format expected by binary PBM images."""

    __slots__ = 'rows', 'columns', 'data'
    
    def __init__(self, rows, columns):
        self.rows = rows
        self.columns = columns
        col_len = columns//8 + (columns%8 > 0)
        self.data = [bytearray(col_len) for _ in range(rows)]

    def __getitem__(self, row_column):
        row, column = row_column
        col_index, col_offset = divmod(column, 8)
        
        return (self.data[row][col_index] >> (7 - col_offset)) & 1

    def __setitem__(self, row_column, bit):
        row, column = row_column
        col_index, col_offset = divmod(column, 8)
        
        if bit:
            self.data[row][col_index] |= 1 << (7 - col_offset)
        else:
            self.data[row][col_index] &= ~(1 << (7 - col_offset))


class PBM:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.pixels = BitMatrix(height, width)

    def set_pixels(self, xs, ys):
        for row, col in zip(ys, xs):
            self.pixels[row, col] = 1

    def save_image(self, filename):
        if not filename.endswith('.pbm'):
            filename += '.pbm'

        with open(filename, 'wb') as f:
            f.write(b'P4\n')
            f.write(f'{self.width} {self.height}\n'.encode())
            f.write(b''.join(self.pixels.data))
