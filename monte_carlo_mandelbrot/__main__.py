# This file is part of Monte Carlo Mandelbrot, a module which generates random points in the Mandelbrot set.
# Copyright (C) 2022  Christian Calderon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# To contact the author you can send an email to calderonchristian73 AT gmail DOT com,
# or you can make an issue on the gitlab repository: https://gitlab.com/ChrisCalderon/monte-carlo-mandelbrot
import numpy as np
from pathlib import Path
from math import isclose
from monte_carlo_mandelbrot.pbm import PBM
from monte_carlo_mandelbrot import mc_mandelbrot
from argparse import (
    ArgumentParser,
    ArgumentDefaultsHelpFormatter
)


def parse_args():
    module_name = Path(__file__).parent.name
    parser = ArgumentParser(
        prog=f'python -m {module_name}',
        description='Create black and white images of the Mandelbrot set.',
        formatter_class=ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        '-o',
        '--output',
        default='mandelbrot.pbm',
        help='Specify the output filename.'
    )
    parser.add_argument(
        '--min-real',
        type=float,
        default=-2.0,
        help='The minimum value of the real part.'
    )
    parser.add_argument(
        '--max-real',
        type=float,
        default=1.0,
        help='The maximum value of the real part.'
    )
    parser.add_argument(
        '--min-imag',
        type=float,
        default=-1.5,
        help='The minimum value of the imaginary part.'
    )
    parser.add_argument(
        '--max-imag',
        type=float,
        default=1.5,
        help='The maximum value of the imaginary part.'
    )
    parser.add_argument(
        '--iterations',
        type=int,
        default=100,
        help='The number of iterations of z**2 + c.'
    )
    parser.add_argument(
        '--max-points',
        type=int,
        default=10**7,
        help='The number of random points to test.'
    )
    parser.add_argument(
        '--pixel-width',
        type=int,
        default=2000,
        help='The pixel width of the image created.'
    )
    parser.add_argument(
        '--pixel-height',
        type=int,
        default=2000,
        help='The pixel height of the image created.'
    )
    return parser.parse_args()


def main():
    args = parse_args()

    real_delta = args.max_real - args.min_real
    imag_delta = args.max_imag - args.min_imag

    if not isclose(real_delta/imag_delta, args.pixel_width/args.pixel_height):
        print('Warning: the numeric aspect ratio does not match the image aspect ratio.')
        print('The resulting image may look stretched.')

    points = mc_mandelbrot(
        args.min_real,
        args.max_real,
        args.min_imag,
        args.max_imag,
        args.iterations,
        args.max_points
    )

    points_reals = np.real(points)
    points_imags = np.imag(points)

    xs = (args.pixel_width*(points_reals - args.min_real)/real_delta).astype(int)
    ys = (args.pixel_height*(args.max_imag - points_imags)/imag_delta).astype(int)

    image = PBM(args.pixel_width, args.pixel_height)
    image.set_pixels(xs, ys)
    image.save_image(args.output)


main()
