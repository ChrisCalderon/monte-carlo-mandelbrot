# Monte Carlo Mandelbrot

This module provides a function for generating a list of random points in the Mandelbrot set, as well as a CLI interface for generating black and white images of the Mandelbrot set.

The image format used is PBM (in binary mode) for it's simplicity, and Numpy is used to do the number crunching. Numpy is the only external dependency required.

This code is developed using Python 3.10, and I can't guarantee that it works on other Python versions.

This code is licensed under the GPLv3.

The CLI for this module can be used with this command: `python -m monte_carlo_mandelbrot`

Here is a copy of the current help output:

```
usage: python -m monte_carlo_mandelbrot [-h] [-o OUTPUT] [--min-real MIN_REAL]
                                        [--max-real MAX_REAL]
                                        [--min-imag MIN_IMAG]
                                        [--max-imag MAX_IMAG]
                                        [--iterations ITERATIONS]
                                        [--max-points MAX_POINTS]
                                        [--pixel-width PIXEL_WIDTH]
                                        [--pixel-height PIXEL_HEIGHT]

Create black and white images of the Mandelbrot set.

options:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Specify the output filename. (default: mandelbrot.pbm)
  --min-real MIN_REAL   The minimum value of the real part. (default: -2.0)
  --max-real MAX_REAL   The maximum value of the real part. (default: 2.0)
  --min-imag MIN_IMAG   The minimum value of the imaginary part. (default:
                        -2.0)
  --max-imag MAX_IMAG   The maximum value of the imaginary part. (default:
                        2.0)
  --iterations ITERATIONS
                        The number of iterations of z**2 + c. (default: 100)
  --max-points MAX_POINTS
                        The number of random points to test. (default:
                        10000000)
  --pixel-width PIXEL_WIDTH
                        The pixel width of the image created. (default: 2000)
  --pixel-height PIXEL_HEIGHT
                        The pixel height of the image created. (default: 2000)

```
